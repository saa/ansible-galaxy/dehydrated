# Dehydrated ACME client

This Ansible role installs and configures
[dehydrated](https://github.com/dehydrated-io/dehydrated), a flexible ACME
client written in Bash. The software is installed and configured with an
optional list of domains to manage, and a cron job is scheduled for obtaining
and renewing certificates.

## Installation

Add the following to a `requirements.yml` or `requirements.yaml` file in your
Ansible project and run `ansible-galaxy install -r requirements.yaml`:

```yaml
roles:
  - name: dehydrated
    scm: git
    src: https://gitlab.uvm.edu/saa/ansible-galaxy/dehydrated.git
```


## Usage

### Let's Encrypt certificates

To manage [Let's Encrypt](https://letsencrypt.org) certificates, place a
section like this in your playbook:

```yaml
- name: Set up dehydrated ACME client
  hosts: all
  roles:
    - role: dehydrated
      dehydrated_install_type: "lets-encrypt"
      dehydrated_domains:
        - site1.uvm.edu
        - site2.uvm.edu
```

A web server should be configured on port `80` to pass requests for the URI
prefix `/.well-known` on the specified domains to `/var/www/dehydrated` before
`dehydrated` can be run to obtain certificates.

### InCommon/Sectigo certificates

To manage InCommon/Sectigo certificates, create an ACME management account from
the [InCommon/Sectigo
portal](https://cert-manager.com/customer/InCommon?locale=en#acme), get its key
values, and then place a section like this in your playbook:

```yaml
- name: Set up ACME client
  hosts: all
  roles:
    - role: dehydrated
      dehydrated_install_type: "incommon"
      dehydrated_account_keyid: "[...]"
      dehydrated_account_hmac: "[...]"
      dehydrated_domains:
        - site1.uvm.edu
        - site2.uvm.edu
```

After the playbook has run, `dehydrated` can be used to obtain certificates and
does not require a web server with special configuration.

### Additional options

Full argument documentation is available via `ansible-doc -t role dehydrated`.
